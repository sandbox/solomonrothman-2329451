INTRODUCTION
------------

The Backbone Libs module adds the backbone.js, underscore.js, backbone.localStorage and twig.js libraries and inserts them onto specified paths.

REQUIREMENTS
------------
This module requires the following modules:
 * Libraries (https://www.drupal.org/project/libraries)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Download backbone.js from http://backbonejs.org/, extract and put backbone.js and backbone-min.js in libraries/backbone, so the actual libraries are available at libraries/backbone/backbone-min.js and libraries/backbone/backbone.js
 * Download underscore.js from http://underscorejs.org/, extract and put underscore.js and underscore-min.js in libraries/underscore
 * Download twig.js from https://github.com/justjohn/twig.js/, extract it and put twig.js and twig.min.js in libraries/twig.js
 * Download backbone.localStorage.js from https://github.com/jeromegn/Backbone.localStorage, and put backbone.localStorage-min.js and backbone.localStorage.js in libraries/backbone.localStorage (notice the capital S on Storage).
   
CONFIGURATION
-------------
 * Visit admin/config/development/backbone-lib and choose the production (compressed) or development version and check off each library you want included on the specified paths. Be careful, dependencies are not checked so don't enable just libraries that require dependencies for example if you check backbone.localStorage, make sure backbone.js is also checked.
